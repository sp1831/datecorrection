import readTime
import cv2


def datecorrection(path,x1,x2,y1,y2):
    video = cv2.VideoCapture(path)
    max_length = int(video.get(cv2.CAP_PROP_FRAME_COUNT))
    return readTime.readTime(path,0,x1,y1,y2-y1,x2-x1),readTime.readTime(path,max_length-1,x1,y1,y2-y1,x2-x1)
